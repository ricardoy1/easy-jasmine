# README #

To run the tests simply pull the repository and open RunTheTestsHere.html with your favourite browser.
You will see the tests running and hopefully all of them are going to pass by printing a green line for each of them.

### What is this repository for? ###

* This repository is intended to give you a rough and basic idea of how to use the Jasmine testing framework in simple examples.

### How do I get set up? ###

* There is no requirement other than allowing Javascript to run in your browser.

### Who do I talk to? ###

* If you have any questions: ricardoy1@gmail.com
http://rsyoussef.com/easy-jasmine/